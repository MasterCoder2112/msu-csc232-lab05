/**
* CSC232 Data Structures with C++
* Missouri State University, Spring 2017.
*
* @file    EnhancedArrayBag.h
* @authors Jim Daehn <jdaehn@missouristate.edu>
*          Alexander Byrd <zz2112@live.missouristate.edu>
		   Trenton Sauer <ts191@live.missouristate.edu>
		   Travis Bushong <bushong1843@live.missouristate.edu>
* @brief   Header file for an array-based implementation of the EnhancedArrayBag.
*
* @copyright 2017 Byrd and Partners, Education Springfield MO
*/

#ifndef ENHANCED_ARRAY_BAG_
#define ENHANCED_ARRAY_BAG_

#include "ArrayBag.h"

template<class T>
class EnhancedArrayBag : public ArrayBag<T> {
public:
    virtual EnhancedArrayBag<T> unionWithBag(const EnhancedArrayBag<T>& other) const;
	virtual EnhancedArrayBag<T> intersectionWithBag(const EnhancedArrayBag<T>& other) const;
	virtual EnhancedArrayBag<T> differenceWithBag(const EnhancedArrayBag<T>& other) const;
}; // end ArrayBag

#include "EnhancedArrayBag.cpp"

#endif