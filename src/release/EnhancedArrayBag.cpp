/**
* CSC232 Data Structures with C++
* Missouri State University, Spring 2017.
*
* @file    EnhancedArrayBag.cpp
* @authors Jim Daehn <jdaehn@missouristate.edu>
*          Alexander Byrd <zz2112@live.missouristate.edu>
*          Trenton Sauer <ts191@live.missouristate.edu>
*          Travis Bushong <bushong1843@live.missouristate.edu>
*
* @brief   Actual implementation of the EnhancedArrayBag.
*
* @copyright 2017 Byrd, Education. Springfield MO
*/

#include "EnhancedArrayBag.h"
#include <vector>
#include <cstddef>

/**
* @title unionWithBag
* @brief Takes a bag object reference sent in, and the current bag object
* that makes the method call, and uses the toVector() method to make a
* vector of the entries in each bag, so that it can be iterated through and
* all the entries of each bag can be unionized into one bag to be returned.
* It uses the add method to construct the new unionized bag
* @parameters other(Bag object reference)
* @pre A Bag object reference is sent in to be unionized with the bag
* object making the method call
* @post Creates a new bag consisting of the entries in each bag, and
* returns it
* @return A Bag with a union of both bags entries
*/
template<class T>
EnhancedArrayBag<T> EnhancedArrayBag<T>::unionWithBag(const EnhancedArrayBag<T>& other) const {

	//This bags vector
	std::vector<T> vectorA = this -> toVector();

	//Other bags vector
	std::vector<T> vectorB = other.toVector();
	EnhancedArrayBag<T> temp;

	for (int i = 0; i < vectorB.size(); i++) {
		temp.add(vectorB.at(i));
	}

	for (int i = 0; i < vectorA.size(); i++) {
		temp.add(vectorA.at(i));
	}

	return temp;
}

/**
* @title intersectionWithBag
* @brief Takes a bag (EnhancedArrayBag<T>) object reference sent in, and the current
* bag object that makes the method call, and uses the toVector() to convert the current
* bag object into a vector, and then iterates through that vector and
* uses the contains() method to figure out which entries are contained in
* both bags. The "other" bags contents are stored in a seperate bag with a different memory location
* "copyOfOther" so that the original bag "other" is not changed. 
* Each time contains returns true for both, it is added into a
* 3rd bag using the add() method to be returned as the intersection of
* entries of the two bags. Also whenever a match is found, that particular
* entry in copyOfOther is removed so that there are no duplicate intersections using
* the remove() method.
* @parameters other(Bag object reference)
* @pre A Bag object reference is sent in containing entries
* @post Creates a new bag consisting of entries that intersect (are the same)
* in both bags
* @return A Bag with the intersection of both bags entries
*/
template<class T>
EnhancedArrayBag<T> EnhancedArrayBag<T>::intersectionWithBag(const EnhancedArrayBag<T>& other) const {

	//Same as above method
	std::vector<T> vectorA = this -> toVector();
	std::vector<T> vectorB = other.toVector();

	EnhancedArrayBag<T> temp;
	EnhancedArrayBag<T> copyOfOther;

	for (int i = 0; i < vectorB.size(); i++) {
		copyOfOther.add(vectorB.at(i));
	}

	//Interates through all entries seeing if there is intersections
	for (int i = 0; i < vectorA.size(); i++) {

		//If both bags contain this entry
		if (copyOfOther.contains(vectorA.at(i))) {

			//Add intersection to temporary bag
			temp.add(vectorA.at(i));

			//Remove one occurance of this entry in copyOfOther
			copyOfOther.remove(vectorA.at(i));
		}
	}
	

	return temp;
}

/**
* @title differenceWithBag
* @brief Takes a EnhancedArrayBag<T> (which we'll shorten to a bag for doxygen purposes)
* object reference sent in, and the current bag object
* that makes the method call, and uses the toVector() to convert the current
* bag object (this) into a vector, and then iterates through that vector and
* uses the contains() method to figure out which entries are contained in
* both bags. Each time contains returns false, the means that it is a different
* entry than what is in copyOfOther, and it is added using the add() method into the
* new bag to be returned. If contains() sends back true, remove that object from
* copyOfOther so that for example if the current bag has a,b,b,c and copyOfOther only
* has b,d,e, then the second b in the current bag would be a difference and not be
* considered an intersection as well. copyOfOther is a bag that is not equal to
* "other" sent in, in terms of memory location, but is equal in terms of what it
* stores, so it can be used effectively in this method without changing the contents
* of other.
* @parameters other(Bag object reference)
* @pre A Bag object reference is sent in containing entries
* @post Creates a new bag consisting of entries that are different
* in the bag making the method call
* @return A Bag with the difference between both bags entries
*/
template<class T>
EnhancedArrayBag<T> EnhancedArrayBag<T>::differenceWithBag(const EnhancedArrayBag<T>& other) const {

	//Same as other methods
	std::vector<T> vectorA = this -> toVector();
	std::vector<T> vectorB = other.toVector();

	EnhancedArrayBag<T> temp;
	EnhancedArrayBag<T> copyOfOther;

	for (int i = 0; i < vectorB.size(); i++) {
		copyOfOther.add(vectorB.at(i));
	}

	//Interates through all entries seeing if there is intersections
	for (int i = 0; i < vectorA.size(); i++) {

		//If both bags contain this entry
		if (copyOfOther.contains(vectorA.at(i))) {

			//Remove one occurance of this entry in copyOfOther
			copyOfOther.remove(vectorA.at(i));
		}
		else {

			//Add to temporary bag if it is a difference
			temp.add(vectorA.at(i));
		}
	}

	return temp;
}